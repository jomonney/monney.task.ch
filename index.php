<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php require_once('template/head.php'); ?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require_once('template/header.php'); ?>
			<main class="container off-canvas-content" data-off-canvas-content>
        <div class="container">
					<ul class="tasklist">
						<li class="tasklist-header">
							<span class="tasklist-item-id">
                ID
							</span>
							<span class="tasklist-item-priority hide-for-medium-only hide-for-small-only">
								Priority
							</span>
							<span class="tasklist-item-description">
								Description
							</span>
							<span class="tasklist-item-creator  hide-for-medium-only hide-for-small-only">
                <a href="?tri=cree_asc"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
            		Creator
            		<a href="?tri=cree_desc"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
							</span>
							<span class="tasklist-item-assigned">
								Assigned to
							</span>
							<span class="tasklist-item-due">
                <a href="?tri=date_asc"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
            		Deadline
            		<a href="?tri=date_desc"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
							</span>
							<span class="tasklist-item-actions">
								Actions
							</span>
						</li>

		<?php
$order =	null;
if(isset($_GET['tri'])) {
	switch($_GET['tri']) {
		case 'cree_asc':
			$order = 'ORDER BY created_by ASC';
			break;
		case 'cree_desc':
			$order = 'ORDER BY created_by DESC';
			break;
		case 'date_asc':
			$order = 'ORDER BY due_at ASC';
			break;
		case 'date_desc':
			$order = 'ORDER BY due_at DESC';
			break;
		default:
			$order = null;
			break;
	}
}
	          $query = $db -> prepare('SELECT
																		task.id,
																		description,
																		created_at,
																		due_at,
																		priority,
																		status,
																		creator.id as creator_id,
																		creator.name as creator_name,
																		assignee.id as assignee_id,
																		assignee.name as assignee_name
																		FROM task
																		INNER JOIN user as creator on created_by = creator.id
                                    INNER JOIN user as assignee on assigned_to = assignee.id
                                    '.
            												$order);
						$query -> execute();
	          while($data = $query -> fetch()):
	          ?>
						<li class="tasklist-item<?php if($data['status'] == 'close'): ?> tasklist-item-close<?php endif; ?>">
	            <span class="tasklist-item-id">
	              <?php echo $data['id']; ?>
	            </span>
							<span class="tasklist-item-priority hide-for-medium-only hide-for-small-only">
	              <?php echo $data['priority']; ?>
	            </span>
	            <span class="tasklist-item-description">
	              <?php echo $data['description']; ?>
	            </span>
							<span class="tasklist-item-creator  hide-for-medium-only hide-for-small-only">
								<?php echo $data['creator_name']; ?>
							</span>
							<span class="tasklist-item-assigned">
								<?php echo $data['assignee_name']; ?>
							</span>
	            <span class="tasklist-item-due">
	              <?php echo $data['due_at']; ?>
	            </span>
	            <span class="tasklist-item-actions">
								<a href="edit.php?id=<?php echo $data['id']; ?>">
	                <i class="fa fa-pencil-square" aria-hidden="true"></i>
	              </a>
	              <a href="#" data-delete="<?php echo $data['id']; ?>">
                  <i class="fa fa-window-close" aria-hidden="true"></i>
	              </a>
								<a href="#" data-done="<?php echo $data['id']; ?>">
	                <i class="fa fa-check-square" aria-hidden="true"></i>
	              </a>
	            </span>
	          </li>
	          <?php endwhile; ?>
					</ul>
				</div>
			</main>
			<?php require('template/footer.php'); ?>
		</div>
  </body>
</html>
