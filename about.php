<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php require_once('template/head.php'); ?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require_once('template/header.php'); ?>

			<main class="container off-canvas-content" data-off-canvas-content>
        <div class="about">
          <h1>My Task</h1>
          <p>MyTask est une application web de gestion de tâche créer dans le cadre d’un projet d’Icomunication. Cette application a été créée grâce à plusieurs languages de programmations.
          </br></br>J'ai eu environ 2 mois pour créer cette application. La réalisation de cette application fut très compliqué car contenant de nombreux langage totalement inconnu mais, cela fut tout de même très enrichissant et m'a bien aidé a progresser en programmation web.</p>
        </div>

			</main>
			<?php require_once('template/footer.php'); ?>
		</div>
  </body>
</html>
