<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php require_once('template/head.php'); ?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require_once('template/header.php'); ?>

			<main class="container off-canvas-content" data-off-canvas-content>
        <table class="pdf">
	<tr>
		<td>
			<iframe src="etc/pdf/manuel_technique.pdf" width="800" height="700" align="middle"></iframe>
		</td>
	</tr>
</table>

			</main>

			<?php require_once('template/footer.php'); ?>
		</div>
  </body>
</html>
