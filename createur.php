<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php require_once('template/head.php'); ?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require_once('template/header.php'); ?>

			<main class="container off-canvas-content" data-off-canvas-content>
        <div class="about">
          <h1>Qui suis-je ?</h1>
          <p>Je suis Jordane Monney 22 ans. </br></br>
            J'habite actuellement à Châbles et je suis étudiant en Telecomunication à la Haute École d'ingénierie et d'architecture de Fribourg.</br></br>
            Avant de commencer l'école d'ingénieur j'ai fait un apprentissage de Médiamaticien à Swisscom puis j'ai effectué mon Service militaire dans les forces aériennes.</br></br>
            J'aime la course, le football et l'informatique en général.
        </div>
			</main>
			<?php require_once('template/footer.php'); ?>
		</div>
  </body>
</html>
