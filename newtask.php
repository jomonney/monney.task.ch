<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php require_once('template/head.php'); ?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require_once('template/header.php');	?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
          <div class="container-newtask">
					<h1 class="page-title">New Task</h1>
					<form method="post" action="update.php" class="small-12 medium-6 collumn">
            <label>Description</label>
            <textarea name="description" rows="6"></textarea>
            <label>Priority</label>
            <select name="priority">
              <?php for($i = 1; $i <= 5; $i++): ?>
                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
              <?php endfor; ?>
            </select>
            <label>Deadline</label>
            <input type="date" name="due_at"/>
						<label>Assigned to</label>
						<select name="assigned_to">
							<?php
							$query = $db -> query('SELECT * FROM user');
							while($data =	$query -> fetch()):
							?>
								<option value="<?php echo $data['id']; ?>"><?php echo $data['name']; ?></option>
							<?php
							endwhile;
							?>
            </select>
            <input type="submit" value="Submit" class="button"/>
	        </form>
				</div>
      </div>
			</main>

			<?php require_once('template/footer.php'); ?>
		</div>
  </body>
</html>
