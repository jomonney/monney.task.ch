<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<header class="top row expanded">
	<div class="top-bar">
		<div class="top-bar-title">
			<button class="menu-icon hide-for-large" type="button" data-toggle="offCanvas"></button>
			<a href="index.php">My Task <i class="fa fa-check-square-o" aria-hidden="true"></i></a>
		</div>
		<div class="top-bar-right">
			<?php
			$query =	$db -> prepare('SELECT * FROM user WHERE id = ?');
			$query -> execute(array($_SESSION['userid']));
			$data = $query -> fetch();
			?>
			<ul class="dropdown menu" data-dropdown-menu>
				<li>
					<img src="assets/img/<?php echo $_SESSION['userid']; ?>.jpg" class="image-user" />
					<ul class="menu">
						<li><a href="edituser.php?id=<?php echo $_SESSION['userid']; ?>"><?php echo $data['name']; ?></a></li>
						<li><a href="logout.php">Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</header>
<nav class="nav off-canvas-absolute position-left reveal-for-large" id="offCanvas" data-off-canvas>
	<ul class="vertical menu">
		<li><a href="newtask.php">New Task</a></li>
		<li><a href="users-list.php">User list</a></li>
		<li><a href="newuser.php">New user</a></li>
		<li><a href="presentation.php">Manuel utilisateur</a></li>
		<li><a href="technique.php">Manuel technique</a></li>
		<li><a href="createur.php">Presentation du créateur</a></li>
		<li><a href="about.php">About</a></li>
	</ul>
</nav>
